# Internet Secured Operating System

Internet Secured Operationg System is a minimal operating system based on Minix kernel and NetBSD tools with few things in mind: simplicity, security, it's using NetBSD package manger, the code should be clean code and vendor lock should avoided so that you can create your own bootable image from sources.

This can be compiled for PC, mobile, Internet of Things devices.

You need a UNIX like distribution with gcc compiler installed for building bootable image, you run the following command in terminal for x86 architecture:

<b>bash releasetools/x86_hdimage.sh</b>

It could be found a build(bootable image) at the following address<br>
<a href="https://drive.google.com/open?id=0B-gkRvBr1uNIaFdpak9wNTVQeVk">Internet Secured Operating System</a>
